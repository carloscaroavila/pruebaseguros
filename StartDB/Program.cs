﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Context;
using BusinessLogic.Entities;
namespace StartDB
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new InsuranceContext())
            {
                //Se agrega un elemento para obligar a EF a realizar la actualización del contexto
                Risk r = new Risk() { LevelName = "alto" };
                ctx.Risks.Add(r);
                ctx.SaveChanges();
            }
        }
    }
}
