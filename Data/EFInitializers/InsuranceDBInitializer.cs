﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Context;
using System.Data.Entity;
using BusinessLogic.Entities;
namespace Data.EFInitializers
{
    class InsuranceDBInitializer: CreateDatabaseIfNotExists<InsuranceContext>
    {
        protected override void Seed(InsuranceContext context)
        {
            //Agrego vendedores por defecto
            IList<Seller> defaultSellers = new List<Seller>();
            defaultSellers.Add(new Seller() { Name = "Monica", LastName = "Giraldo", Password = "123456", UserName = "monica.giraldo@yopmail.com" });
            defaultSellers.Add(new Seller() { Name = "David", LastName = "Caro", Password = "654321", UserName = "david.caro@yopmail.com" });
            context.Sellers.AddRange(defaultSellers);


            IList<Risk> defaultRisks = new List<Risk>();
            defaultRisks.Add(new Risk() { LevelName = "bajo" });
            defaultRisks.Add(new Risk() { LevelName = "medio" });
            defaultRisks.Add(new Risk() { LevelName = "medio-alto" });
            //defaultRisks.Add(new Risk() { LevelName = "alto"});
            context.Risks.AddRange(defaultRisks);


            IList<Client> defaultClients = new List<Client>();
            defaultClients.Add(new Client { Name = "Jesus", LastName = "Delrio" });
            defaultClients.Add(new Client { Name = "Felipe", LastName = "Erazo" });
            context.Clients.AddRange(defaultClients);

            //Se adicionan tipos de accidentes
            IList<Coverage> defaultCoverages = new List<Coverage>();
            defaultCoverages.Add(new Coverage { Name = "Terremoto", Rate = 50 });
            defaultCoverages.Add(new Coverage { Name = "Incendio", Rate = 30});
            defaultCoverages.Add(new Coverage { Name = "Robo", Rate = 70});
            defaultCoverages.Add(new Coverage { Name = "Pérdida", Rate = 40 });
            context.Coverages.AddRange(defaultCoverages);
            
            //Se adicionan Polizas por defecto
            IList<Insurance> defaultInsurances = new List<Insurance>();
            List<Coverage> ListCoverageInsurance = new List<Coverage>();
            //Terremoto y Robo
            ListCoverageInsurance.Add(defaultCoverages[0]);
            ListCoverageInsurance.Add(defaultCoverages[2]); 
            //Con Riesgo bajo
            defaultInsurances.Add(new Insurance { Risk = defaultRisks[0], Coverages = ListCoverageInsurance, Name="Poliza con Bajo riesgo para T y R",Price = 5000,Description = " Poliza contra terreoto y robo en zonas con poco movimiento telurico con 20 meses", TimeFrame=20});

            ListCoverageInsurance = new List<Coverage>();
            //Incendio
            ListCoverageInsurance.Add(defaultCoverages[1]);
            defaultInsurances.Add(new Insurance { Risk = defaultRisks[1], Coverages = ListCoverageInsurance,Name="Poliza con medio riesgo para I",Price = 57000, Description = "Poliza contra incendios, en climas calidos (Riesgo medio)", TimeFrame = 5});

            ListCoverageInsurance = new List<Coverage>();
            ListCoverageInsurance.Add(defaultCoverages[3]);

            defaultInsurances.Add(new Insurance { Risk = defaultRisks[2], Coverages = ListCoverageInsurance, Name = "Poliza con medio-alto riesgo para P", Price = 58000, Description = "Poliza contra pérdida, con indices medios de criminalidad (Riesgo medio-alto)", TimeFrame = 7 });

            context.Insurances.AddRange(defaultInsurances);


            //Relación de Polizas con Clientes

            context.ClientInsurances.Add(new ClientInsurance() { Client = defaultClients[0], Insurance = defaultInsurances[0], ValidSince = DateTime.Now, Status = true });
            context.ClientInsurances.Add(new ClientInsurance() { Client = defaultClients[0], Insurance = defaultInsurances[1], ValidSince = DateTime.Now, Status = true });
            context.ClientInsurances.Add(new ClientInsurance() { Client = defaultClients[0], Insurance = defaultInsurances[2], ValidSince = DateTime.Now, Status = true });

            context.ClientInsurances.Add(new ClientInsurance() { Client = defaultClients[1], Insurance = defaultInsurances[0], ValidSince = DateTime.Now, Status = true });
            context.ClientInsurances.Add(new ClientInsurance() { Client = defaultClients[1], Insurance = defaultInsurances[1], ValidSince = DateTime.Now, Status = false });

            base.Seed(context);
        }
    }
}
