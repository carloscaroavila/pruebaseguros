﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
using System.Data.Entity;
using BusinessLogic.IRepositories;
using Data.Context;
namespace Data.Repository
{
    class RiskRepository: Repository<Risk>, IRiskRepository
    {
        public RiskRepository(InsuranceContext context):base(context)
        {
            
        }
        public InsuranceContext InsuranceContext
        {
            get {return Context as InsuranceContext; }
        }
    }
}
