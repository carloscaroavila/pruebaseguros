﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
using System.Data.Entity;
using BusinessLogic.IRepositories;
using Data.Context;
namespace Data.Repository
{
    public class CoverageRepository: Repository<Coverage>, ICoverageRepository
    {
        public CoverageRepository(InsuranceContext context):base(context)
        {

        }
        public InsuranceContext InsuranceContext { get { return Context as InsuranceContext; } }

        public IEnumerable<Coverage> GetCoveragesByInsurance(int idInsurance)
        {
            //Retorna todas las Coverturas de una poliza en particular
            return InsuranceContext.Coverages
                   .Where(s => s.Insurances.Any(c => c.InsuranceId == idInsurance)).ToList();
        }
    }
}
