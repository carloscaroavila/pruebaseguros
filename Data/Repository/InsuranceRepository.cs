﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
using System.Data.Entity;
using BusinessLogic.IRepositories;
using Data.Context;
namespace Data.Repository
{
    public class InsuranceRepository: Repository<Insurance>, IInsuranceRepository
    {
        public InsuranceRepository(InsuranceContext context):base(context)
        {
        }
        public InsuranceContext InsuranceContext { get { return Context as InsuranceContext; } }

        public IEnumerable<Insurance> GetActiveInsurancesRelatedToClient(int IdClient)
        {
            return InsuranceContext.Insurances
                        .Where(y => y.ClientInsurances.Any(ci => ci.ClientID == IdClient && ci.Status == true ));
        }

        public IEnumerable<Insurance> GetInsurancesRelatedToClient(int IdClient)
        {
            return InsuranceContext.Insurances
                                    .Where(y => y.ClientInsurances.Any(ci => ci.ClientID == IdClient));
        }

        public IEnumerable<Insurance> GetInsurancesWithCoverages()
        {
            return InsuranceContext.Insurances
                                    .Include(insure => insure.Coverages).ToList<Insurance>();
        }

        public Insurance GetInsuranceWithCoverage(int idInsurance)
        {
            return InsuranceContext.Insurances
                                    .Include(insure => insure.Coverages)
                                    .Where(i => i.InsuranceId == idInsurance).First<Insurance>();
        }
    }
}
