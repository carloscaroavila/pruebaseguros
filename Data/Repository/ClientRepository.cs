﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
using System.Data.Entity;
using BusinessLogic.IRepositories;
using Data.Context;
namespace Data.Repository
{
    public class ClientRepository : Repository<Client>, IClientRepository
    {
        public ClientRepository(InsuranceContext context) : base(context)
        {

        }
        public List<Client> GetClientsWithActiveInsurances()
        {
            return InsuranceContext.Clients
                   .Include(c => c.ClientInsurances)
                   .ToList();
        }
        public Client GetClientWithActiveInsurances(int id)
        {
            return InsuranceContext.Clients
                                    .Include(x => x.ClientInsurances)
                                    .Where(y => y.ClientID == id).First<Client>();
        }
        public InsuranceContext InsuranceContext { get { return Context as InsuranceContext; } }

      
    }
}
