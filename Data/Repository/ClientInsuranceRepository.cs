﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
using BusinessLogic.IRepositories;
using Data.Context;
using System.Data.Entity;
namespace Data.Repository
{
    class ClientInsuranceRepository: Repository<IClientInsuranceRespository>,IClientInsuranceRespository
    {
        public ClientInsuranceRepository(InsuranceContext context):base(context)
        {

        }
        public List<ClientInsurance> GetClientInsuranceActiveInsurances()
        {

            return InsuranceContext.ClientInsurances
                    .Include(c => c.Client)
                    .Where(c => c.Status == true)
                    .ToList();
        }


        public InsuranceContext InsuranceContext
        {
            get { return Context as InsuranceContext; }
        }

      
    }
}
