﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
using Data.EFInitializers;
namespace Data.Context
{
    public class InsuranceContext: DbContext
    {
        public InsuranceContext()
        :base("name=InsuranceContext")
        {
            //Se establece configuración para realizar cargue exhaustivo
            this.Configuration.LazyLoadingEnabled = false;
            //Se agrega el inicializador personalizado, para realizar cargue
            Database.SetInitializer(new InsuranceDBInitializer());
        }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<ClientInsurance> ClientInsurances { get; set; }

        public virtual DbSet<Coverage> Coverages { get; set; }
        public virtual DbSet<Insurance> Insurances { get; set; }
        public virtual DbSet<Risk> Risks { get; set; }
        public virtual DbSet<Seller> Sellers { get; set; }
    }
}
