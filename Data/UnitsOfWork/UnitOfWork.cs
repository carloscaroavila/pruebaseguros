﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.IRepositories;
using BusinessLogic.IUnitsOfWork;
using Data.Context;
using Data.Repository;
namespace Data.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly InsuranceContext _context;
        public UnitOfWork(InsuranceContext context)
        {
            _context = context;
            Coverages =new CoverageRepository(_context);
            Clients = new ClientRepository(_context);
            Insurances = new InsuranceRepository(_context);
            Sellers = new SellerRepository(_context);
            Risks = new RiskRepository(_context);
        }
        public ICoverageRepository Coverages{ get; private set;}

        public IInsuranceRepository Insurances { get; private set; }

        public IClientRepository Clients { get; private set; }

        public ISellerRepository Sellers { get; private set; }

        public IRiskRepository Risks { get; private set; }
        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
