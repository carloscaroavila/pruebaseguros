﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Data.UnitsOfWork;
using Data.Context;
using BusinessLogic.Entities;

namespace PruebasSeguros.UnitTests
{
    [TestClass]
    public class CoverageTests
    {
        [TestMethod]
        public void CanStoreANewCoverage_CoverageIsFull_ReturnsTrue()
        {
            //Arrange
            using (var unitOfWork = new UnitOfWork(new InsuranceContext()))
            {
                
                unitOfWork.Coverages.Add(new Coverage() { Name = "Hello World", Rate= 50 });
                //Act
                int i=unitOfWork.Complete();
                //Acert
                Assert.AreEqual(i,1);
            }



        }
    }
}
