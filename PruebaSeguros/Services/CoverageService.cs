﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic.IRepositories;
using BusinessLogic.IUnitsOfWork;
using BusinessLogic.Entities;
using Data.Repository;
using Crosscut;
using Data.Context;
using Data.UnitsOfWork;
using BusinessLogic.Commnads.Coverage;
using System.Collections.ObjectModel;

namespace PruebaSeguros.Services
{
    public class CoverageService
    {
        private IUnitOfWork _unitOfWork;
        public CoverageService(IUnitOfWork UnitOfWork)
        {
            _unitOfWork = UnitOfWork;
        }
        public ApplicationResponse<Coverage> LoadCoverages()
        {
            try
            {
                List<Coverage> CoverageList;
                using (_unitOfWork)
                {
                    CoverageList = _unitOfWork.Coverages.GetAll().ToList();
                }
                return new ApplicationResponse<Coverage> { Success = true, Message = "Ok", ObjectList = CoverageList };
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Coverage>() { Success = false, Message = e.Message };
            }
        }

        public ApplicationResponse<Coverage> ChangeCovertureRelatedInsurance(List<CoverageCommand> resultado)
        {
            try{
                using (_unitOfWork)
                {
                    //Tomar la poliza a cambiar
                    if(resultado!=null && (resultado.Count>0))
                    {
                        Insurance s = _unitOfWork.Insurances.GetInsuranceWithCoverage(resultado[0].InsuranceId);

                        bool validacionPorcentajePorRiesgoActiva = false;
                        //Verificar reglar del negocio, para la que si el riesgo es alto... las coverturas
                        //no se podrán asociar a la poliza
                        if(s.RiskId == 4) //Si el riesgo es alto
                        {
                            resultado.ForEach(delegate (CoverageCommand c) {
                                if(_unitOfWork.Coverages.Get(c.CoverageId).Rate >= 50)
                                {
                                    validacionPorcentajePorRiesgoActiva = true;
                                }
                            });
                            
                        }

                        if (!validacionPorcentajePorRiesgoActiva)
                        {
                            if (s.Coverages != null && (s.Coverages.Count > 0))
                            {
                                List<Coverage> qw = new List<Coverage>(s.Coverages);

                                //Se eliminan las coverturas que ya no están
                                qw.ForEach(delegate (Coverage i)
                                {
                                    if (!resultado.Any(x => x.CoverageId == i.CoverageId))
                                    {
                                        s.Coverages.Remove(i);
                                    }
                                });

                            }

                            //Adicionar las nuevas coverturas
                            bool insuraceWithOutCoverages = s.Coverages.Count == 0;
                            resultado.ForEach(delegate (CoverageCommand i) {

                                Coverage foundCoverage = null;
                                if (!insuraceWithOutCoverages)
                                    foundCoverage = s.Coverages.Where(x => x.CoverageId == i.CoverageId).First();
                                if (foundCoverage == null)
                                {
                                    foundCoverage = _unitOfWork.Coverages.Get(i.CoverageId);
                                    s.Coverages.Add(foundCoverage);
                                }
                            });


                            _unitOfWork.Complete();
                            return new ApplicationResponse<Coverage> { Success = true, Message = "Coverturas asociadas con exito" };
                        }
                        else
                        {
                            return new ApplicationResponse<Coverage> { Success = false, Message = "Las coverturas con más de 50% para esta poliza no son permitidas" };
                        }
                        
                    }
                    else
                    {
                        using (_unitOfWork)
                        {
                            //_unitOfWork.Insurances
                        }
                        return new ApplicationResponse<Coverage> { Success = true, Message = "Coverturas asociadas con exito" };
                    }
                   
                }

            }catch (Exception e)
            {
                return new ApplicationResponse<Coverage> { Success = false, Message = e.Message };
            }
        }

        public ApplicationResponse<Coverage> RemoveCoveragesFromInsurance(int idPoliza)
        {
            using (_unitOfWork)
            {
                try
                {
                    Insurance insuranceWithCoverages=_unitOfWork.Insurances.GetInsuranceWithCoverage(idPoliza);
                    if(insuranceWithCoverages.Coverages != null && insuranceWithCoverages.Coverages.Count > 0)
                    {
                        insuranceWithCoverages.Coverages.Clear();
                    }
                    _unitOfWork.Complete();
                    return new ApplicationResponse<Coverage> { Success = true, Message = "Coverturas eliminadas con exito" };
                }
                catch (Exception e)
                {
                    return new ApplicationResponse<Coverage> { Success = false, Message = e.Message };
                }
            }
        }

        public ApplicationResponse<Coverage> LoadCovergarBy(int id)
        {
            try
            {
                Coverage FoundCoverage;
                using (_unitOfWork)
                {
                    FoundCoverage = _unitOfWork.Coverages.Get(id);
                }
                if (FoundCoverage != null)
                    return new ApplicationResponse<Coverage>() { Success = true, Message = "Ok", Object = FoundCoverage };
                else
                    return new ApplicationResponse<Coverage>() { Success = true, Message = "Covertura no encontrada" };
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Coverage>() { Success = false, Message = "Error tomando una poliza por id" };
            }
        }
        public ApplicationResponse<Coverage> ChargeCoverturesByInsurance(int idInsurance)
        {
            try
            {
                List<Coverage> FoundCoverages;
                using (_unitOfWork)
                {
                    FoundCoverages = _unitOfWork.Coverages.GetCoveragesByInsurance(idInsurance).ToList<Coverage>();
                }
                return new ApplicationResponse<Coverage>() { Success = true, Message = "Ok", ObjectList = FoundCoverages };
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Coverage>() { Success = false, Message = "Error tomando una poliza por id" };
            }
        }
    }
}