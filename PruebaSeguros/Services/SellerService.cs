﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic.IRepositories;
using BusinessLogic.IUnitsOfWork;
using BusinessLogic.Entities;
using Data.Repository;
using Crosscut;
using Data.Context;
using Data.UnitsOfWork;
using BusinessLogic.Commnads.Seller;

namespace PruebaSeguros.Services
{
    public class SellerService
    {
        private IUnitOfWork _unitOfWork;

        public SellerService(IUnitOfWork UnitOfWork)
        {
            _unitOfWork = UnitOfWork;
        }
        public ApplicationResponse<Seller> Login(InsuranceCommand loginCommand)
        {
            try
            {
                string authenticationCookie;
                List<String> validationErrors;
                Seller sell;
                //Validate the command
                using (_unitOfWork)
                {
                    validationErrors = loginCommand.Validate(_unitOfWork);

                    //If have validation errors then interrupt the action and return those errors
                    if (validationErrors.Count() > 0)
                        return new ApplicationResponse<Seller>() { Success = false, Message = "Informacion Invalida", Messages = validationErrors };

                    //Take user 
                    List<Seller> usuarios = _unitOfWork.Sellers.GetAll().ToList<Seller>();

                    sell = usuarios.Where(x => x.UserName == loginCommand.UserName).FirstOrDefault();

                    //Building string for cookie
                     authenticationCookie = sell.SellerId.ToString() + "|" + sell.UserName;
                }
                return new ApplicationResponse<Seller>() { Success = true, Message = authenticationCookie, Messages = validationErrors, Object = sell };
                
               

            }
            catch (Exception ex)
            {
                return new ApplicationResponse<Seller>() { Success = false, Message = "There was an error in the login", Messages = new List<string>(), };
            }
        }
    }
}