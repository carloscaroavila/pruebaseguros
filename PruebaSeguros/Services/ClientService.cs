﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic.IRepositories;
using BusinessLogic.IUnitsOfWork;
using BusinessLogic.Entities;
using Data.Repository;
using Crosscut;
using Data.Context;
using Data.UnitsOfWork;

namespace PruebaSeguros.Services
{
    public class ClientService
    {
        private IUnitOfWork _unitOfWork;
        public ClientService(IUnitOfWork UnitOfWork)
        {
            _unitOfWork = UnitOfWork;
        }
        public ApplicationResponse<Client> loadClients()
        {
            try
            {
                List<Client> ClientList;
                using (_unitOfWork)
                {
                    ClientList = _unitOfWork.Clients.GetAll().ToList();
                }
                return new ApplicationResponse<Client> { Success = true, Message = "Ok", ObjectList = ClientList };
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Client>() { Success = false, Message = e.Message };
            }
        }
    }
}