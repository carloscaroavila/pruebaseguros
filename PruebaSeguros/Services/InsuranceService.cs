﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic.IRepositories;
using BusinessLogic.IUnitsOfWork;
using BusinessLogic.Entities;
using Data.Repository;
using Crosscut;
using Data.Context;
using Data.UnitsOfWork;
using BusinessLogic.Commnads.Insurance;
using BusinessLogic.Mappers;

namespace PruebaSeguros.Services
{
    public class InsuranceService
    {
        private IUnitOfWork _unitOfWork;
        public InsuranceService(IUnitOfWork UnitOfWork)
        {
            _unitOfWork = UnitOfWork;
        }
        public ApplicationResponse<Insurance> LoadInsurances()
        {
            try
            {
                using (_unitOfWork)
                {
                    var InsurancesList = _unitOfWork.Insurances.GetAll().ToList();
                    return new ApplicationResponse<Insurance> { Success = true, Message = "Ok", ObjectList = InsurancesList };
                }

            }
            catch (Exception e)
            {
                return new ApplicationResponse<Insurance>() { Success = false, Message = e.Message };
            }
        }
        public ApplicationResponse<Insurance> LoadInsuranceBy(int id)
        {
            try
            {
                Insurance FoundInsurance;
                using (_unitOfWork)
                {
                    FoundInsurance = _unitOfWork.Insurances.Get(id);
                }
                if (FoundInsurance != null)
                    return new ApplicationResponse<Insurance>() { Success = true, Message = "Ok", Object = FoundInsurance };
                else
                    return new ApplicationResponse<Insurance>() { Success = true, Message = "Poliza no encontrada" };
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Insurance>() { Success = false, Message = "Error tomando una poliza por id" };
            }
        }

        public ApplicationResponse<Insurance> GetInsurancesByClient(int idClient)
        {
            try
            {
                List<Insurance> ListInsurancesByClient;
                using (_unitOfWork)
                {
                    ListInsurancesByClient = _unitOfWork.Insurances.GetActiveInsurancesRelatedToClient(idClient).ToList();
                   
                }
                return new ApplicationResponse<Insurance> { Success = true, Message = "Ok", ObjectList = ListInsurancesByClient };
            }
            catch(Exception e)
            {
                return new ApplicationResponse<Insurance> { Success = false, Message = e.Message };
            }
        }

        public ApplicationResponse<Insurance> ChangeClientRelatedInsurances(List<SimpleInsuranceCommand> resultado)
        {
            try
            {
                using (_unitOfWork)
                {
                    //Tomar la poliza a cambiar
                    if (resultado != null && (resultado.Count > 0))
                    {
                        Client s = _unitOfWork.Clients.GetClientWithActiveInsurances((int)resultado[0].ClientId);
                        if (s.ClientInsurances!= null && (s.ClientInsurances.Count > 0))
                        {
                            List<ClientInsurance> qw = s.ClientInsurances.ToList();

                            //Se eliminan las polizas que ya no están
                            qw.ForEach(delegate (ClientInsurance i)
                            {
                                if (!resultado.Any(x => x.ClientId == i.ClientID && x.InsuranceId == i.InsuranceId))
                                {
                                    s.ClientInsurances.Remove(i);
                                    //s.ClientInsurances.Clear();
                                }
                            });

                        }

                        //Adicionar las nuevas polizas
                        bool clientWithOutInsurances = s.ClientInsurances.Count == 0;
                        resultado.ForEach(delegate (SimpleInsuranceCommand i) {

                            ClientInsurance foundInsurance = null;
                            if (!clientWithOutInsurances)
                                
                                if (s.ClientInsurances.Where(x => x.ClientID == i.ClientId && x.InsuranceId == i.InsuranceId).Any())
                                {
                                    foundInsurance = s.ClientInsurances.Where(x => x.ClientID == i.ClientId && x.InsuranceId == i.InsuranceId).First();
                                }
                                
                            if (foundInsurance == null)
                            {
                                Insurance tmpInsurance = _unitOfWork.Insurances.Get((int)i.InsuranceId);
                                Client tmpClient = _unitOfWork.Clients.Get((int)i.ClientId);
                                ClientInsurance clientInstTmp = new ClientInsurance();
                                clientInstTmp.Client = tmpClient;
                                clientInstTmp.Insurance = tmpInsurance;
                                clientInstTmp.Status = true;
                                clientInstTmp.ValidSince = DateTime.Now;
                                s.ClientInsurances.Add(clientInstTmp);
                            }
                        });
                        _unitOfWork.Complete();
                        return new ApplicationResponse<Insurance> { Success = true, Message = "Coverturas asociadas con exito" };
                    }
                    else
                    {
                        using (_unitOfWork)
                        {
                            //_unitOfWork.Insurances
                        }
                        return new ApplicationResponse<Insurance> { Success = true, Message = "Coverturas asociadas con exito" };
                    }

                }
            }
            catch(Exception e)
            {
                return new ApplicationResponse<Insurance> { Success = false, Message = "No se logró asociar coberturas." };
            }
        }

        public ApplicationResponse<Insurance> removeInsurancesFromClient(int idClient)
        {
            try
            {
                using (_unitOfWork)
                {
                    Client clientWithInsurances = _unitOfWork.Clients.GetClientWithActiveInsurances(idClient);
                    clientWithInsurances.ClientInsurances.Clear();
                    _unitOfWork.Complete();
                    return new ApplicationResponse<Insurance>() { Success = true, Message = "Se eliminar las polizas del cliente correctamente" };
                }
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Insurance>() { Success = false, Message=e.Message};
            }
           
        }

        public ApplicationResponse<Insurance> CrearPoliza(InsuranceCommand insuranceCommand)
        {
            try
            {
                using (_unitOfWork)
                {
                    //Se valida que los campos del formulario
                    var validationErrors = insuranceCommand.Validate(_unitOfWork);
                    if (validationErrors.Count() > 0)
                        return new ApplicationResponse<Insurance>() { Success = false, Message = "Informacion Invalida", Messages = validationErrors };
                    //Se transforma el objeto capturado a una instancia de este entorno
                    var insurance = InsuranceMapper.InsuranceCommand_To_Insurance(insuranceCommand);
                    _unitOfWork.Insurances.Add(insurance);
                    _unitOfWork.Complete(); 

                    return new ApplicationResponse<Insurance>() { Success = true, Message = "Poliza creada con exito" };
                }
            }catch(Exception e)
            {
                return new ApplicationResponse<Insurance>() { Success = false, Message = e.Message };
            }
        }
        public ApplicationResponse<Insurance> ChangeInsurance(InsuranceCommand insuranceCommand)
        {
            try
            {
                using (_unitOfWork)
                {
                    
                    //Se valida que los campos del formulario
                    var validationErrors = insuranceCommand.Validate(_unitOfWork);
                    if (validationErrors.Count() > 0)
                        return new ApplicationResponse<Insurance>() { Success = false, Message = "Informacion Invalida", Messages = validationErrors };
                    //Se toma la poliza desde la base de datos con el Id de la instancia command
                    Insurance insuranceToEdit=_unitOfWork.Insurances.Get((int)insuranceCommand.InsuranceId);

                    //Se transforma el objeto capturado a una instancia de este entorno
                    InsuranceMapper.InsuranceCommand_To_Insurance_update(ref insuranceToEdit, insuranceCommand);
                    _unitOfWork.Complete();

                    return new ApplicationResponse<Insurance>() { Success = true, Message = "Poliza Actualizzada con exito" };
                }
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Insurance>() { Success = false, Message = e.Message };
            }
        }
        public ApplicationResponse<Insurance> deleteInsurance(int idInsurance)
        {
            try
            {
                using (_unitOfWork)
                {
                    Insurance almostGoneInsurance =_unitOfWork.Insurances.Get(idInsurance);
                    if(almostGoneInsurance != null)
                    {
                        _unitOfWork.Insurances.Remove(almostGoneInsurance);
                        _unitOfWork.Complete();
                        return new ApplicationResponse<Insurance>() { Success = true, Message = "Póliza eliminada con exito" };
                    }else
                    {
                        return new ApplicationResponse<Insurance>() { Success = false, Message = "La Póliza no se pudo eliminar" };
                    }
                  
                }
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Insurance>() { Success = false, Message = "Error en el sistema" };
            }
        }
    }
 }
