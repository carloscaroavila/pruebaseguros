﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic.IRepositories;
using BusinessLogic.IUnitsOfWork;
using BusinessLogic.Entities;
using Data.Repository;
using Crosscut;
using Data.Context;
using Data.UnitsOfWork;

namespace PruebaSeguros.Services
{
    public class RiskService
    {
        private IUnitOfWork _unitOfWork;
        public RiskService(IUnitOfWork UnitOfWork)
        {
            _unitOfWork = UnitOfWork;
        }
        public ApplicationResponse<Risk> LoadRisks()
        {
            try
            {
                List<Risk> RiskList;
                using (_unitOfWork)
                {
                    RiskList = _unitOfWork.Risks.GetAll().ToList();
                }
                return new ApplicationResponse<Risk> { Success = true, Message = "Ok", ObjectList = RiskList };
            }
            catch (Exception e)
            {
                return new ApplicationResponse<Risk>() { Success = false, Message = e.Message };
            }
        }
    }
}