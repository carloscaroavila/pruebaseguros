﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Crosscut;
using BusinessLogic.Entities;
using PruebaSeguros.Services;
using BusinessLogic.IUnitsOfWork;
using Data.UnitsOfWork;
using Data.Context;
using BusinessLogic.Commnads.Insurance;

namespace PruebaSeguros.Controllers
{
    public class InsuranceController : BaseApiController
    {
        private InsuranceService _insuranceService;

        public InsuranceController()
        {
            _insuranceService = new InsuranceService(new UnitOfWork(new InsuranceContext()));
        }

        public ApplicationResponse<Insurance> GetAll()
         {
            return _insuranceService.LoadInsurances();
        }
        [HttpGet]
        public ApplicationResponse<Insurance> GetPoliza(int idPoliza)
        {
            return _insuranceService.LoadInsuranceBy(idPoliza);
        }
        [HttpPost]
        [ActionName("CrearPoliza")]
        public ApplicationResponse<Insurance> CrearPoliza(InsuranceCommand PolizaCommand)
        {
            return _insuranceService.CrearPoliza(PolizaCommand);
        }
        [HttpPost]
        [ActionName("ActualizarPoliza")]
        public ApplicationResponse<Insurance> ActualizarPoliza(InsuranceCommand pCommnad)
        {
            return _insuranceService.ChangeInsurance(pCommnad);
        }
        [HttpGet]
        [ActionName("GetInsurancesByClient")]
        public ApplicationResponse<Insurance> GetInsurancesByClient(int IdClient)
        {
            try
            {
                return _insuranceService.GetInsurancesByClient(IdClient);
            }catch(Exception e)
            {
                return new ApplicationResponse<Insurance>() { Success = false, Message ="Something went wrong" };
            } 
            
        }
        [HttpPost]
        public ApplicationResponse<Insurance> EditarClientesPoliza(List<SimpleInsuranceCommand> ListInstuancesCommand)
        {
            return _insuranceService.ChangeClientRelatedInsurances(ListInstuancesCommand);
        }
        [HttpDelete]
        [ActionName("EliminarClientesPoliza")]
        public ApplicationResponse<Insurance> EliminarClientesPoliza(int IdClient)
        {

            return _insuranceService.removeInsurancesFromClient(IdClient);
        }
        [HttpDelete]
        [ActionName("EliminarPoliza")]
        public ApplicationResponse<Insurance> EliminarPoliza(int idPoliza)
        {
            return _insuranceService.deleteInsurance(idPoliza);
        }
    }
}