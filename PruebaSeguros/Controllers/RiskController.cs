﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Crosscut;
using BusinessLogic.Entities;
using PruebaSeguros.Services;
using BusinessLogic.IUnitsOfWork;
using Data.UnitsOfWork;
using Data.Context;
namespace PruebaSeguros.Controllers
{
    public class RiskController : BaseApiController
    {
        private RiskService _riskService;

        public RiskController()
        {
            _riskService = new RiskService(new UnitOfWork(new InsuranceContext()));
        }

        public ApplicationResponse<Risk> GetAll()
        {
            return _riskService.LoadRisks();
        }
    }
}