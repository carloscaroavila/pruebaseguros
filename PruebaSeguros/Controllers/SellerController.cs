﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Security;
using Crosscut;
using BusinessLogic.Entities;
using BusinessLogic.Commnads.Seller;
using PruebaSeguros.Services;
using Data.UnitsOfWork;
using Data.Context;

namespace PruebaSeguros.Controllers
{
    public class SellerController : BaseApiController
    {
        private SellerService _sellerService;

        public SellerController(){
            _sellerService = new SellerService(new UnitOfWork(new InsuranceContext()));
        }
        [HttpPost]
        public ApplicationResponse<Seller> Login(InsuranceCommand sellerCommand)
        {
            var result = _sellerService.Login(sellerCommand);
            if (result.Success == true)
                FormsAuthentication.SetAuthCookie(result.Message, false);

            return result;
        }
        [HttpGet]
        public ApplicationResponse<Seller> Logout()
        {
            FormsAuthentication.SignOut();
            return new ApplicationResponse<Seller>() { Success = true, Message = "Sesion finalizada" }; ;
        }

        [HttpGet]
        [Authorize]
        public ApplicationResponse<Seller> IsAuthenticated()
        {
            return new ApplicationResponse<Seller>() { Success = true };
        }
    }
}