﻿using BusinessLogic.Entities;
using Crosscut;
using Data.Context;
using Data.UnitsOfWork;
using PruebaSeguros.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PruebaSeguros.Controllers
{
    public class ClientController : BaseApiController
    {
        private ClientService _clientService;

        public ClientController()
        {
            _clientService = new ClientService(new UnitOfWork(new InsuranceContext()));
        }
        public ApplicationResponse<Client> GetAll()
        {
            return _clientService.loadClients();
        }
    }
}