﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PruebaSeguros.Controllers
{
    public class BaseApiController: ApiController
    {
        protected long _idUsuario
        {
            get
            {
                long idUsuario = 0;
                try
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                        idUsuario = Convert.ToInt64(HttpContext.Current.User.Identity.Name.Split('|')[0]);
                }
                catch (Exception ex)
                {
                }
                return idUsuario;
            }
        }

        protected string _nombreUsuario
        {
            get
            {
                string nombreUsuario = string.Empty;
                try
                {
                    if (HttpContext.Current.Request.IsAuthenticated)
                        nombreUsuario = HttpContext.Current.User.Identity.Name.Split('|')[1];
                }
                catch (Exception ex)
                {
                }
                return nombreUsuario;
            }
        }
    }
}