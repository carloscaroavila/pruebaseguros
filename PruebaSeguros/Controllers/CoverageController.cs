﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Crosscut;
using BusinessLogic.Entities;
using PruebaSeguros.Services;
using BusinessLogic.IUnitsOfWork;
using Data.UnitsOfWork;
using Data.Context;
using BusinessLogic.Commnads.Coverage;

namespace PruebaSeguros.Controllers
{
    public class CoverageController : BaseApiController
    {
        private CoverageService _coverageService;

        public CoverageController()
        {
            _coverageService = new CoverageService(new UnitOfWork(new InsuranceContext()));
        }

        public ApplicationResponse<Coverage> GetAll()
        {
            return _coverageService.LoadCoverages();
        }
        public ApplicationResponse<Coverage> GetByInsurance(int IdPoliza)
        {
            return _coverageService.ChargeCoverturesByInsurance(IdPoliza);
        }
        public ApplicationResponse<Coverage> EditarCoverturaPoliza(List<CoverageCommand> resultado)
        {
            return _coverageService.ChangeCovertureRelatedInsurance(resultado);
        }
        [HttpDelete]
        public ApplicationResponse<Coverage> EliminarCoverturasPoliza(int idPoliza)
        {
            return _coverageService.RemoveCoveragesFromInsurance(idPoliza);
        }

    }
}