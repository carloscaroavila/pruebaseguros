﻿angular.module('AppSeguros').controller('AddPolizaController', ['$scope', '$stateParams', 'PolizaService', 'GeneralService', 'SweetAlert', AddPolizaController]);
function AddPolizaController($scope, $stateParams, PolizaService, GeneralService, SweetAlert) {

    $scope.ListaPolizas = PolizaService.ListaPolizas;
    $scope.ListaPolizasSelecionadas = PolizaService.ListaPolizasDeCliente;
    $scope.ListaPolizasDeCliente = PolizaService.ListaPolizasDeCliente;

    PolizaService.ColectarPolizas();
    PolizaService.ColectarPolizasPorCliente($stateParams.IdClient, function () {
        $scope.isChecked = function (PolizaId) {
            if ($scope.ListaPolizasDeCliente.length != 0) {
                var boolFound = false;
                for (var i = 0; i < $scope.ListaPolizasDeCliente.length; i++) {
                    if ($scope.ListaPolizasDeCliente[i].InsuranceId == PolizaId) {
                        boolFound = true;
                    }
                }
                return boolFound;
            } else {
                return false;
            }
        };
    });
    $scope.ActualizarListaSelecionado = function (Poliza) {
        var found = $scope.ListaPolizasSelecionadas.find(function (element) {
            return element.InsuranceId == Poliza.InsuranceId;
        });

        if (found == null) {//Elemento no seleccioando
            $scope.ListaPolizasSelecionadas.push(Poliza);
        } else {
            var index = $scope.ListaPolizasSelecionadas.findIndex(function (element2) {
                return element2.InsuranceId == Poliza.InsuranceId;
            });
            if (index > -1) {
                $scope.ListaPolizasSelecionadas.splice(index, 1);
            }
        }
    };

    $scope.ActualizarPolizas = function () {


        for (var i = 0; i < $scope.ListaPolizasSelecionadas.length; i++) {
            //se agrega un atributo adicional para relacionar con poliza en backend          
            $scope.ListaPolizasSelecionadas[i].ClientId = $stateParams.IdClient;
        }

        if ($scope.ListaPolizasSelecionadas.length > 0) {
            $scope.ListaCoverturaCommand = $scope.ListaPolizasSelecionadas;
            PolizaService.EditarClientesPoliza($scope);
        } else {
            debugger;
            $scope.IdClient = $stateParams.IdClient;
            PolizaService.EliminarClientesPoliza($scope);
        }

    };



};
