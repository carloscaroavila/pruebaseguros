﻿angular.module('AppSeguros').controller('ClientesController', ['$scope', 'ClienteService', 'GeneralService', 'SweetAlert', ClientesController]);
function ClientesController($scope, ClienteService, GeneralService, SweetAlert) {
    $scope.ListaClientes = ClienteService.ListaClientes;
    ClienteService.ColectarClientes();

    $scope.AsociarPolizas = function (idClient) {
        GeneralService.GoToUrl('AddPolizas/' + idClient);
    };
};


