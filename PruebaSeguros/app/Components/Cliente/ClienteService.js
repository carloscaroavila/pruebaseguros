﻿angular.module('AppSeguros').factory('ClienteService', ['$http', 'GeneralService', ClienteService]);
function ClienteService($http, GeneralService) {
    var service = {};

    //Clientes
    service.ListaClientes = [];
    service.ColectarClientes = function () {
        $http({
            method: 'GET',
            url: 'api/client/GetAll'
        }).success(function (response) {
            service.ListaClientes.length = 0;
            if (response.Success) {
                angular.forEach(response.ObjectList, function (value, key) {
                    service.ListaClientes.push(value);
                });
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    service.SelectedClient = {};
    service.GetClient = function (idCliente) {
        $http({
            method: 'GET',
            url: 'api/client/GetClient',
            params: {
                idCliente: idCliente
            }
        }).success(function (response) {
            if (response.Success) {
                Object.assign(service.SelectedClient, response.Object);
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    return service;
};
