﻿angular.module('AppSeguros').controller('LoginController', ["$scope", 'SellerService', 'GeneralService', LoginController]);
function LoginController($scope, SellerService, GeneralService) {
    $scope.SellerCommand = { UserName: "", Password: "" };
    $scope.ErrorMessage = "";
    $scope.ValidationErrors = [];
    $scope.IsPosting = false;


    $scope.Login = function () {
        SellerService.Login($scope);
    };

};


