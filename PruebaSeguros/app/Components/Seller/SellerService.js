﻿
angular.module('AppSeguros').factory('SellerService', ['$http', '$location', "GeneralService", SellerService]);
function SellerService($http, $location, GeneralService) {
    var service = {};

    service.User = { SellerId: '' };
    service.Authentication = { IsAuth: false };

    service.Login = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        $http({
            method: 'POST',
            url: 'api/Seller/Login',
            data: $ParentScope.SellerCommand
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            debugger;
            if (response.Success == true) {
                $ParentScope.Login = true;
                service.User = response.Object;
                service.Authentication.IsAuth = true;
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.GoToState('Home');
            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                service.User = {};
                service.Authentication.IsAuth = false;
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            service.User = {};
            service.Authentication.IsAuth = false;
            $ParentScope.Message = 'Problems with the system.';
            $ParentScope.Messages = [];
        });
    };

    service.Logout = function () {
        $http({
            method: 'GET',
            url: 'api/Seller/Logout'
        }).success(function (response) {
            if (response.Success == true) {
                service.User = {};
                service.Authentication.IsAuth = false;
                GeneralService.GoToState('Login');
            } else {
                service.User = {};
                service.Authentication.IsAuth = false;
                GeneralService.GoToState('Login');
            }
        });
    };

    service.CheckIfIsAuthenticated = function () {
        $http({
            method: 'GET',
            url: 'api/Seller/IsAuthenticated'
        }).success(function (response) {
            if (response.Success == true) {
                service.Authentication.IsAuth = true;
                service.User = response.Object;
            } else {
                service.Authentication.IsAuth = true;
                service.User = {};
            }
        });
    };

    service.CheckIfIsAuthenticatedWithPremisse = function () {
        return $http({
            method: 'GET',
            url: 'api/Seller/IsAuthenticated'
        });
    };

    return service;
};
