﻿angular.module('AppSeguros').factory('CoverturaService', ['$http', 'GeneralService', CoverturaService]);
function CoverturaService($http, GeneralService) {
    var service = {};

    //Covertura
    service.ListaCoverturas = [];
    service.ColectarCoverturas = function () {
        $http({
            method: 'GET',
            url: 'api/coverage/GetAll'
        }).success(function (response) {
            service.ListaCoverturas.length = 0;
            if (response.Success) {
                angular.forEach(response.ObjectList, function (value, key) {
                    service.ListaCoverturas.push(value);
                });
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    service.ListaCoverturasDePoliza = [];
    service.ColectarCoverturasPorPoliza = function (IdPoliza , callback) {
        $http({
            method: 'GET',
            url: 'api/coverage/GetByInsurance',
            params:{
                IdPoliza:IdPoliza
            }
        }).success(function (response) {
            service.ListaCoverturasDePoliza.length = 0;
            if (response.Success) {
                angular.forEach(response.ObjectList, function (value, key) {
                    service.ListaCoverturasDePoliza.push(value);
                }
                );
                callback();
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };

    service.SelectedCovertura = {};
    service.GetCovertura = function (idCovertura) {
        $http({
            method: 'GET',
            url: 'api/coverage/GetCovertura',
            params: {
                idCovertura: idCovertura
            }
        }).success(function (response) {
            if (response.Success) {
                Object.assign(service.SelectedCovertura, response.Object);
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    //$scope.ListaCoverturasSelecionadas

    service.EditarCoverturasPoliza = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        $http({
            method: 'POST',
            url: 'api/coverage/EditarCoverturaPoliza',
            data: $ParentScope.ListaCoverturaCommand
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            if (response.Success == true) {
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.ShowDefaultSuccessMessage();
                GeneralService.GoToUrl('/Poliza');

            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                GeneralService.ShowSweetAlert("Error", $ParentScope.Message);
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            $ParentScope.Message = "Problemas en el servidor";
            $ParentScope.Messages = [];

            GeneralService.ShowDefaultErrorMessage();
        });
    };

    service.EliminarCoverturasPoliza = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        $http({
            method: 'DELETE',
            url: 'api/coverage/EliminarCoverturasPoliza',
            params: {
                IdPoliza: $ParentScope.IdPoliza
            }
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            if (response.Success == true) {
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.ShowDefaultSuccessMessage();
                GeneralService.GoToUrl('/Poliza');

            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            $ParentScope.Message = "Problemas en el servidor";
            $ParentScope.Messages = [];

            GeneralService.ShowDefaultErrorMessage();
        });
    };

    service.EditarCovertura = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        $http({
            method: 'POST',
            url: 'api/insurance/EditarCovertura',
            data: $ParentScope.CoverturaCommand
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            if (response.Success == true) {
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.ShowDefaultSuccessMessage();
                GeneralService.GoToUrl('/Covertura');

            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            $ParentScope.Message = "Problemas en el servidor";
            $ParentScope.Messages = [];

            GeneralService.ShowDefaultErrorMessage();
        });
    };
    service.EliminarCovertura = function (idCovertura) {
        $http({
            method: 'DELETE',
            url: 'api/insurance/EliminarCovertura',
            params: {
                idCovertura: idCovertura
            }
        }).success(function (response) {
            if (response.Success) {
                GeneralService.ShowDefaultSuccessMessage();
                service.ColectarCoverturas();
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    service.CrearCovertura = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        $http({
            method: 'POST',
            url: 'api/insurance/CrearCovertura',
            data: $ParentScope.CoverturaCommand
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            if (response.Success == true) {
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.ShowDefaultSuccessMessage();
                GeneralService.GoToUrl('/Covertura');

            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            $ParentScope.Message = "Problemas en el servidor";
            $ParentScope.Messages = [];

            GeneralService.ShowDefaultErrorMessage();
        });
    };
    return service;
};
