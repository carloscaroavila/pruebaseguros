﻿angular.module('AppSeguros').factory('RiesgoService', ['$http', 'GeneralService', RiesgoService]);
function RiesgoService($http, GeneralService) {
    var service = {};

    //Riesgos
    service.ListaRiesgos = [];
    service.ColectarRiesgos = function () {
        $http({
            method: 'GET',
            url: 'api/risk/GetAll'
        }).success(function (response) {
            service.ListaRiesgos.length = 0;
            if (response.Success) {
                angular.forEach(response.ObjectList, function (value, key) {
                    service.ListaRiesgos.push(value);
                });
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    service.ColectarRiesgosPromise = function (callback) {
        $http({
            method: 'GET',
            url: 'api/risk/GetAll'
        }).success(function (response) {
            service.ListaRiesgos.length = 0;
            if (response.Success) {
                angular.forEach(response.ObjectList, function (value, key) {
                    service.ListaRiesgos.push(value);
                });
                callback();
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };

    service.SelectedRiesgo = {};
    service.GetRiesgo = function (idRiesgo) {
        $http({
            method: 'GET',
            url: 'api/riesgo/GetRiesgo',
            params: {
                idPoliza: idRiesgo
            }
        }).success(function (response) {
            if (response.Success) {
                Object.assign(service.SelectedRiesgo, response.Object);
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    return service;
};
