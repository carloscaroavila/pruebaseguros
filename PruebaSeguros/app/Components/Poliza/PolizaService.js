﻿angular.module('AppSeguros').factory('PolizaService', ['$http', 'GeneralService', PolizaService]);
function PolizaService($http, GeneralService) {
    var service = {};

    //Polizas
    service.ListaPolizas = [];
    service.ColectarPolizas = function () {
        $http({
            method: 'GET',
            url: 'api/insurance/GetAll'
        }).success(function (response) {
            service.ListaPolizas.length = 0;
            if (response.Success) {
                angular.forEach(response.ObjectList, function (value, key) {
                    service.ListaPolizas.push(value);
                });
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };

    //Polizas
    service.ListaPolizasDeCliente = [];
    service.ColectarPolizasPorCliente = function (IdClient, callback) {
        $http({
            method: 'GET',
            url: 'api/insurance/GetInsurancesByClient',
            params:{
                IdClient:IdClient
            }
        }).success(function (response) {
            service.ListaPolizasDeCliente.length = 0;
            if (response.Success) {
                angular.forEach(response.ObjectList, function (value, key) {
                    service.ListaPolizasDeCliente.push(value);
                });
                callback();
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };




    service.SelectedPoliza = {};
    service.GetPoliza = function (idPoliza) {
        $http({
            method: 'GET',
            url: 'api/insurance/GetPoliza',
            params: {
                idPoliza: idPoliza
            }
        }).success(function (response) {
            if (response.Success) {
                Object.assign(service.SelectedPoliza, response.Object);
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    service.GetPolizaPromise = function (idPoliza, callback) {
        $http({
            method: 'GET',
            url: 'api/insurance/GetPoliza',
            params: {
                idPoliza: idPoliza
            }
        }).success(function (response) {
            if (response.Success) {
                Object.assign(service.SelectedPoliza, response.Object);
                callback();
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };

    service.EditarPoliza = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        $http({
            method: 'POST',
            url: 'api/insurance/ActualizarPoliza',
            data: $ParentScope.PolizaCommand
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            if (response.Success == true) {
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.ShowDefaultSuccessMessage();
                GeneralService.GoToUrl('/Poliza');

            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            $ParentScope.Message = "Problemas en el servidor";
            $ParentScope.Messages = [];

            GeneralService.ShowDefaultErrorMessage();
        });
    };


    service.EditarClientesPoliza = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        $http({
            method: 'POST',
            url: 'api/insurance/EditarClientesPoliza',
            data: $ParentScope.ListaCoverturaCommand
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            if (response.Success == true) {
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.ShowDefaultSuccessMessage();
                GeneralService.GoToUrl('/Cliente');

            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                GeneralService.ShowSweetAlert("Error", $ParentScope.Message);
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            $ParentScope.Message = "Problemas en el servidor";
            $ParentScope.Messages = [];

            GeneralService.ShowDefaultErrorMessage();
        });
    };

    service.EliminarClientesPoliza = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        debugger;
        $http({
            method: 'DELETE',
            url: 'api/insurance/EliminarClientesPoliza',
            params: {
                IdClient: $ParentScope.IdClient
            }
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            if (response.Success == true) {
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.ShowDefaultSuccessMessage();
                GeneralService.GoToUrl('/Cliente');

            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            $ParentScope.Message = "Problemas en el servidor";
            $ParentScope.Messages = [];

            GeneralService.ShowDefaultErrorMessage();
        });
    };

    service.EliminarPoliza = function (idPoliza) {
        debugger;
        $http({
            method: 'DELETE',
            url: 'api/insurance/EliminarPoliza',
            params: {
                idPoliza: idPoliza
            }
        }).success(function (response) {
            if (response.Success) {
                GeneralService.ShowDefaultSuccessMessage();
                service.ColectarPolizas();
            } else {
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            GeneralService.ShowDefaultErrorMessage();
        });
    };
    service.CrearPoliza = function ($ParentScope) {
        $ParentScope.IsPosting = true;
        $http({
            method: 'POST',
            url: 'api/insurance/CrearPoliza',
            data: $ParentScope.PolizaCommand
        }).success(function (response) {
            $ParentScope.IsPosting = false;
            if (response.Success == true) {
                $ParentScope.Message = '';
                $ParentScope.Messages = [];
                GeneralService.ShowDefaultSuccessMessage();
                GeneralService.GoToUrl('/Poliza');

            } else {
                $ParentScope.Message = response.Message;
                $ParentScope.Messages = response.Messages;
                GeneralService.ShowDefaultErrorMessage();
            }
        }).error(function (error) {
            $ParentScope.IsPosting = false;
            $ParentScope.Message = "Problemas en el servidor";
            $ParentScope.Messages = [];

            GeneralService.ShowDefaultErrorMessage();
        });
    };
    return service;
};
