﻿angular.module('AppSeguros').controller('AddCoverturasController', ['$scope', '$stateParams', 'CoverturaService', 'GeneralService', 'SweetAlert', AddCoverturasController]);
function AddCoverturasController($scope, $stateParams, CoverturaService, GeneralService, SweetAlert) {

    $scope.ListaCoverturas = CoverturaService.ListaCoverturas;
    $scope.ListaCoverturasSelecionadas = CoverturaService.ListaCoverturasDePoliza;
    $scope.ListaCoverturasDePoliza = CoverturaService.ListaCoverturasDePoliza;

    CoverturaService.ColectarCoverturas();
    CoverturaService.ColectarCoverturasPorPoliza($stateParams.IdPoliza, function () {
        $scope.isChecked = function (CoverageId) {
            if ($scope.ListaCoverturasDePoliza.length != 0) {
                var boolFound = false;
                for (var i = 0; i < $scope.ListaCoverturasDePoliza.length; i++) {
                    if ($scope.ListaCoverturasDePoliza[i].CoverageId == CoverageId) {
                        boolFound = true;
                    }
                }
                return boolFound;
            } else {
                return false;
            }
        };
    });


    $scope.ActualizarListaSelecionado = function (Covertura) {
        var found = $scope.ListaCoverturasSelecionadas.find(function (element) {
            return element.CoverageId == Covertura.CoverageId;
        });
       
        if (found == null) {//Elemento no seleccioando
            $scope.ListaCoverturasSelecionadas.push(Covertura);
        } else {
            var index = $scope.ListaCoverturasSelecionadas.findIndex(function (element2) {
                return element2.CoverageId == Covertura.CoverageId;
            });
            if (index > -1) {
                $scope.ListaCoverturasSelecionadas.splice(index, 1);
            }
        }
    };

    $scope.ActualizarCoverturas = function () {


        for (var i = 0; i < $scope.ListaCoverturasSelecionadas.length; i++) {
            //se agrega un atributo adicional para relacionar con poliza en backend          
            $scope.ListaCoverturasSelecionadas[i].InsuranceId = $stateParams.IdPoliza;
        }

        //Se entiende que este es el caso en el que se quieren eliminar todas las coverturas de la poliza
        /*debugger;
        if ($scope.ListaCoverturasSelecionadas.length == 0) {

            Object.assign($scope.ListaCoverturasSelecionadas, $scope.ListaCoverturas[0]);
            $scope.ListaCoverturasSelecionadas.CoverageId = 0;
            $scope.ListaCoverturasSelecionadas.Insurances = null;
            $scope.ListaCoverturasSelecionadas.Name = "";
            $scope.ListaCoverturasSelecionadas.Rate = 0;
            $scope.ListaCoverturasSelecionadas.InsuranceId = $stateParams.IdPoliza;
        }*/
        if ($scope.ListaCoverturasSelecionadas.length > 0) {
            $scope.ListaCoverturaCommand = $scope.ListaCoverturasSelecionadas;
            CoverturaService.EditarCoverturasPoliza($scope);
        } else {
            $scope.IdPoliza = $stateParams.IdPoliza;
            CoverturaService.EliminarCoverturasPoliza($scope);
        }
        
    };
   


};


