﻿angular.module('AppSeguros').controller('PolizasController', ['$scope', 'PolizaService', 'GeneralService', 'SweetAlert', PolizasController]);
function PolizasController($scope, PolizaService, GeneralService, SweetAlert) {
    $scope.ListaPolizas = PolizaService.ListaPolizas;
    PolizaService.ColectarPolizas();

    $scope.IrAEditarPoliza = function (idPoliza) {
        GeneralService.GoToUrl('EditarPoliza/' + idPoliza);
    };

    $scope.AsociarCovertura = function (idPoliza) {
        GeneralService.GoToUrl('AddCoverturas/'+ idPoliza);
    };

    $scope.EliminarPoliza = function (idPoliza) {
        SweetAlert.swal({
            title: "Confirmación",
            text: "Realmente deseas eliminar la poliza?",
            type: "success",
            showCancelButton: true,
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                PolizaService.EliminarPoliza(idPoliza);
            }
        });
    };

};


