﻿angular.module('AppSeguros').controller('EditarPolizaController', ['$scope','$stateParams', 'PolizaService', 'RiesgoService', EditarPolizaController]);
function EditarPolizaController($scope, $stateParams, PolizaService, RiesgoService) {
    $scope.ListaRiesgos = {};
    $scope.PolizaCommand = PolizaService.SelectedPoliza;
    //Se usa para seguir la selección del riesgo desde el select del formulario
    $scope.PolizaCommand2 = PolizaService.SelectedPoliza;
    $scope.Message = '';
    $scope.Messages = [];
    $scope.IsPosting = false;
    RiesgoService.ColectarRiesgos();
    PolizaService.GetPoliza($stateParams.IdPoliza);
    $scope.ListaRiesgos = RiesgoService.ListaRiesgos;

    $scope.EditarPoliza = function () {
        $scope.PolizaCommand.RiskId = $scope.PolizaCommand2.RiskId;
        PolizaService.EditarPoliza($scope);
    };


};