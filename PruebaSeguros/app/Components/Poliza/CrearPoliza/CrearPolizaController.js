﻿angular.module('AppSeguros').controller('CrearPolizaController', ['$scope', 'PolizaService', 'RiesgoService', CrearPolizaController]);
function CrearPolizaController($scope, PolizaService, RiesgoService) {
    debugger;
    $scope.ListaRiesgos = {};
    $scope.InsuraceCommand = {};
    $scope.Message = '';
    $scope.Messages = [];
    $scope.IsPosting = false;
    
    $scope.ListaRiesgos = RiesgoService.ListaRiesgos;
    RiesgoService.ColectarRiesgos();

    $scope.CrearPoliza = function () {
        PolizaService.CrearPoliza($scope);
    };


};