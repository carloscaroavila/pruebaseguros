﻿(function () {
    
    angular.module('AppSeguros', [
        'ui.router',                    // Routing
        'ui.bootstrap',                 // Ui Bootstrap   
        'ngFileUpload',                 // Upload files directive  
        'toaster',                      // Toastr notifications
        'oitozero.ngSweetAlert'         // Alerts and Confirmations
    ])
    .run(['SellerService', function (SellerService) {
        SellerService.CheckIfIsAuthenticated();
    }])
    .run(function ($rootScope, $state, $timeout, $injector, $location, $window, SellerService) {
        //stateChange event
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            $window.scrollTo(0, 0);
            //Authentication Handler
            if (toState.authRequired) {
                if (SellerService.Authentication.IsAuth == false) {
                    event.preventDefault();
                    //alert('User not authenticated');
                    $state.go("Home");
                };
            };

        });
    })
    ;

})();