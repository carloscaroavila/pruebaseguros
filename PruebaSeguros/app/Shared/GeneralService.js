﻿
angular.module('AppSeguros').factory('GeneralService', ['$http', "$timeout", "Upload", "$state", "$location", "$window", '$uibModal', 'toaster', 'SweetAlert', GeneralService]);
function GeneralService($http, $timeout, Upload, $state, $location, $window, $uibModal, toaster, SweetAlert) {
    var service = {};
    //Open modals
    service.OpenModal = function (templateUrl, controller, size) {
        var modalInstance = $uibModal.open({
            templateUrl: templateUrl,
            controller: controller,
            size: size
        });
    };

    //Toastr notifications
    service.ShowSuccessNotification = function (title, message) {
        toaster.pop({
            type: 'success',
            title: title,
            body: message,
            showCloseButton: true,
            timeout: 10000
        });
    };

    service.ShowErrorNotification = function (title, message) {
        toaster.pop({
            type: 'error',
            title: title,
            body: message,
            showCloseButton: true,
            timeout: 10000
        });
    };


    service.ShowDefaultSuccessMessage = function () {
        service.ShowSuccessNotification('Ok', 'Operación exitosa');
    };

    service.ShowDefaultErrorMessage = function () {
        service.ShowErrorNotification('Error', 'Ocurrio un error. Intentalo de nuevo');
    };

    //Alerts and confirmations Sweetalert
    service.ShowSweetAlert = function (title, message) {
        SweetAlert.swal({
            title: title,
            text: message
        });
    };


    //Get querystring object
    service.GetQueryStringObject = function (url) {
        return $location.search();
    };


    //States & Navigation
    service.GoToState = function (stateToGo) {
        $state.go(stateToGo);
    };

    service.Reload = function () {
        $state.reload();
    };

    service.GoToUrl = function (url) {
        $location.path(url);
    };

    service.GetCurrentState = function () {
        return $state.current.name;
    };

    service.GoToUrlInNewTab = function (path) {
        var url = "/#" + path;
        $window.open(url, '_self');
    };

    service.OpenPageInNewTab = function (url) {
        if (url.indexOf('http://') == -1 && url.indexOf('https://') == -1) {
            url = "http://" + url;
        }
        var w = window.open(url, '_blank');
    };

    return service;
};