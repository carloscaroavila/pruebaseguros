﻿angular.module('AppSeguros').factory('AutenticacionInterceptor', ["$q", "$location", "$injector",
    function ($q, $location, $injector) {
        var autenticacionInterceptor = {};

        var responseError = function (rejection) {

            var deferred = $q.defer();
            debugger;
            if (rejection.status === 401) {
                var SellerService = $injector.get("SellerService");
                SellerService.Authentication.IsAuthenticated = false;
                console.log("no esta autenticado");
                $location.path("/Login");
            }
        }

        //autenticacionInterceptor.responseError = responseError;
        return autenticacionInterceptor;
    }
]);

angular.module('AppSeguros').config(function ($httpProvider) {
    $httpProvider.interceptors.push("AutenticacionInterceptor");
});