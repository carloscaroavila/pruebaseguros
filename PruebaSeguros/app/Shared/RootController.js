﻿angular.module('AppSeguros').controller('RootController', ["$scope", "SellerService", RootController]);
function RootController($scope, SellerService) {

    $scope.Logout = function () {
        SellerService.Logout();
    };


    //Logged User
    $scope.User = SellerService.User;
    $scope.$watch(function () { return SellerService.User; }, function (User) {
        $scope.User = SellerService.User;
    }, true);

    $scope.Authentication = SellerService.Authentication;
    $scope.$watch(function () { return SellerService.Authentication; }, function (Authentication) {
        $scope.Authentication = SellerService.Authentication;
    }, true);

};