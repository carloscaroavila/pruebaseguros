﻿
function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/Home");

    $stateProvider
    .state('Home', {
        url: "/Home",
        templateUrl: "App/Components/Home/Home.html",
        controller: "HomeController",
        authRequired: false
    })
    .state('Login', {
        url: "/Login",
        templateUrl: "App/Components/Seller/Login/Login.html",
        controller: "LoginController",
        authRequired: false
    })
    .state('Poliza', {
        url: "/Poliza",
        templateUrl: "App/Components/Poliza/Polizas/Polizas.html",
        controller: "PolizasController",
        authRequired: false
    })
    .state('AddCoverturas', {
        url: "/AddCoverturas/:IdPoliza",
        templateUrl: "App/Components/Poliza/AddCovertura/AddCoverturas.html",
        controller: "AddCoverturasController",
        authRequired: false
    })
    .state('CrearPoliza', {
           url: "/CrearPoliza",
           templateUrl: "App/Components/Poliza/CrearPoliza/CrearPoliza.html",
           controller: "CrearPolizaController",
           authRequired: false
    })
    .state('EditarPoliza', {
        url: "/EditarPoliza/:IdPoliza",
        templateUrl: "App/Components/Poliza/EditarPoliza/EditarPoliza.html",
        controller: "EditarPolizaController",
        authRequired: false
    })
    .state('Cliente', {
        url: "/Cliente",
        templateUrl: "App/Components/Cliente/Clientes/Clientes.html",
        controller: "ClientesController",
        authRequired: false
    })
    .state('AddPolizas', {
        url: "/AddPolizas/:IdClient",
        templateUrl: "App/Components/Cliente/AddPoliza/AddPoliza.html",
        controller: "AddPolizaController",
        authRequired: false
    })
    ;
}

angular.module('AppSeguros').config(config);