﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crosscut
{
    public class ApplicationResponse<T> where T : class
    {
        public bool Success {  get; set; }
        public string Message { get; set; }
        public List<string> Messages { get; set; }
        public T Object { get; set; }
        public List<T> ObjectList { get; set; }
    }
}
