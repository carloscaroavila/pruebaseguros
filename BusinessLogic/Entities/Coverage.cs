﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Entities
{
    public class Coverage
    {
        public int CoverageId { set; get; }
        public string Name { get; set; }
        public int Rate { get; set; }
        public virtual ICollection<Insurance> Insurances { get; set; }
    }
}
