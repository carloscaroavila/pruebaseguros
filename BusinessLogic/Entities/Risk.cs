﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Entities
{
    public class Risk
    {
        public int RiskId {get; set;}
        public string LevelName { get; set; }
    }
}
