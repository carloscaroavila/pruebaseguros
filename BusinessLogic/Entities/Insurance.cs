﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Entities
{
    public class Insurance
    {
        public int InsuranceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int TimeFrame { get; set; }
        public int RiskId { get; set; }
        public ICollection<ClientInsurance> ClientInsurances { get; set; }
        public virtual ICollection<Coverage> Coverages { get; set; }
        public Risk Risk { get; set; }
    }
}
