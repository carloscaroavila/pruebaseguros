﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
namespace BusinessLogic.Entities
{
    public class ClientInsurance
    {
        [Key, Column(Order=0)]
        public int ClientID { get; set; }
        [Key, Column(Order = 1)]
        public int InsuranceId { get; set; }
        public DateTime ValidSince { get; set; }
        public bool Status { get; set; }
        public Insurance Insurance { get; set; }
        public Client Client { get; set; }
    }
}
