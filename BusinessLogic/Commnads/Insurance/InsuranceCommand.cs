﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.IUnitsOfWork;
namespace BusinessLogic.Commnads.Insurance
{
    public class InsuranceCommand
    {


        public int? InsuranceId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int TimeFrame { get; set; }

        public int? RiskId { get; set; }

        public int[] CoverageIds { get; set; }

        public List<string> Validate(IUnitOfWork iUnitOfWork)
        {
            

            
            var validationErrors = new List<string>();
            try
            {
                //Existence Validations 
                if (string.IsNullOrEmpty(this.Name))
                    validationErrors.Add("El nombre de la poliza es obligatorio");
                if (string.IsNullOrEmpty(this.Description))
                    validationErrors.Add("La Descripción es obligatoria");
                if (this.Price==0)
                    validationErrors.Add("El precio es obligatorio");
                if (this.TimeFrame == 0)
                    validationErrors.Add("Los Meses son obligatorios");
                if (this.RiskId == null)
                    validationErrors.Add("Debe seleccionar un Tipo de Riesgo"); 
            }
            catch (Exception ex)
            {
                validationErrors.Add("There was an error validating the object");
            }
            return validationErrors;
        }
    }
}

