﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Commnads.Insurance
{
    public class SimpleInsuranceCommand
    {

        public int? InsuranceId { get; set; }

        public int? ClientId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int TimeFrame { get; set; }

        public int? RiskId { get; set; }
    }
}
