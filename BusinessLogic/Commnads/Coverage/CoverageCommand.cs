﻿using BusinessLogic.IUnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Commnads.Coverage
{
    public class CoverageCommand
    {
        public int InsuranceId { get; set; }
        public int CoverageId { get; set; }
        public string Name { get; set; }

        public int Rate { get; set; }

        public List<string> Validate(IUnitOfWork iUnitOfWork)
        {

            var validationErrors = new List<string>();
            try
            {
                
            }
            catch (Exception ex)
            {
                validationErrors.Add("There was an error validating the object");
            }
            return validationErrors;
        }
    }
}