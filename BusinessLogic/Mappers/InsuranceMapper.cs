﻿using BusinessLogic.Commnads.Insurance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
namespace BusinessLogic.Mappers
{
    public static class InsuranceMapper
    {
        public static Insurance InsuranceCommand_To_Insurance(InsuranceCommand insuranceCommand)
        {
            var Insurance = new Insurance();
            Insurance.Name = insuranceCommand.Name;
            Insurance.Description = insuranceCommand.Description;
            Insurance.Price = insuranceCommand.Price;
            Insurance.TimeFrame = insuranceCommand.TimeFrame;
            Insurance.RiskId = (int)insuranceCommand.RiskId;
            return Insurance;
        }
        public static void InsuranceCommand_To_Insurance_update(ref Insurance ins, InsuranceCommand insuranceCommand)
        {
            ins.Name = insuranceCommand.Name;
            ins.Description = insuranceCommand.Description;
            ins.Price = insuranceCommand.Price;
            ins.TimeFrame = insuranceCommand.TimeFrame;
            ins.RiskId = (int)insuranceCommand.RiskId;
        }
    }
}
