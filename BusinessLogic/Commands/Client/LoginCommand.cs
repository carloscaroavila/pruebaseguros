﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.IRepositories;
using BusinessLogic.IUnitsOfWork;
namespace BusinessLogic.Commands.Client
{
    public class LoginCommand
    {
        public string UserName { get; set; }
        public string Password { get; set; }



        public List<string> Validate(IUnitOfWork _unitOfWork)
        {
            var validationErrors = new List<string>();
            try
            {
                //Existence Validations 
                if (string.IsNullOrEmpty(this.UserName))
                    validationErrors.Add("El usuario es obligatorio");
                if (string.IsNullOrEmpty(this.Password))
                    validationErrors.Add("La contraseña es obligatoria");

                //Size Validations
                if (!string.IsNullOrEmpty(this.UserName) && this.UserName.Length > 50)
                    validationErrors.Add("El usuario debe tener maximo 50 caracteres");
                if (!string.IsNullOrEmpty(this.Password) && this.Password.Length > 50)
                    validationErrors.Add("La contraseña debe tener maximo 50 caracteres");

                //Bussines logic validation
                using (_unitOfWork)
                {
                    List<Entities.Seller> seller = _unitOfWork.Sellers.GetAll().ToList<Entities.Seller>();
                    if (seller.Count <= 0) //Si la lista esta vacia
                        validationErrors.Add("No hay usuarios registrados en la plataforma");
                    else
                    {
                        Entities.Seller usuarioConEsteEmail = seller.Where(x => x.UserName == this.UserName).FirstOrDefault();
                        if (usuarioConEsteEmail == null)//Si no encontro a ningun usuario con ese email
                            validationErrors.Add("Este usuario no existe");
                        else
                        {
                            string contrasenhaEncriptada = this.Password;
                            if (contrasenhaEncriptada != usuarioConEsteEmail.Password)//Si la contraseña digitada en el login y encriptada no coincide con la de la BD
                                validationErrors.Add("Contrasenha invalida");
                        }
                    }
                }
                   
            }
            catch (Exception ex)
            {
                validationErrors.Add("There was an error validating the object");
            }
            return validationErrors;
        }
    }
}
