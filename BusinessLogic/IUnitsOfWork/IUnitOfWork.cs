﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.IRepositories;
namespace BusinessLogic.IUnitsOfWork
{
    public interface IUnitOfWork: IDisposable
    {
        ICoverageRepository Coverages { get; }
        IInsuranceRepository Insurances { get; }
        IClientRepository Clients { get; }

        ISellerRepository Sellers { get; } 
        
        IRiskRepository Risks { get; }
        int Complete();
    }
}
