﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
namespace BusinessLogic.IRepositories
{
    public interface IInsuranceRepository : IRepository<Insurance>
    {
         IEnumerable<Insurance> GetInsurancesWithCoverages();

        Insurance GetInsuranceWithCoverage(int idInsurance);

        IEnumerable<Insurance> GetInsurancesRelatedToClient(int IdClient);

        IEnumerable<Insurance> GetActiveInsurancesRelatedToClient(int IdClient);
    }
}
