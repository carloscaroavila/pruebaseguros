﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
namespace BusinessLogic.IRepositories
{
    public interface IClientRepository : IRepository<Client>
    {
        //Lista los Clientes con seguros activos
        List<Client> GetClientsWithActiveInsurances();
        //Se obtiene un Cliente que conincida con identificador
        //Cargando las polizas asociadas
        Client GetClientWithActiveInsurances(int id);
    }
}
