﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
namespace BusinessLogic.IRepositories
{
    public interface IClientInsuranceRespository: IRepository<IClientInsuranceRespository>
    {
        List<ClientInsurance> GetClientInsuranceActiveInsurances();

    }
}
