﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.Entities;
namespace BusinessLogic.IRepositories
{
    public interface ICoverageRepository: IRepository<Coverage>
    {
        IEnumerable<Coverage> GetCoveragesByInsurance(int idInsurance);
    }
}
